const express = require('express');
const app = express();
const Caesar = require('caesar-salad').Caesar;
const port = 8000;

const key = 'someKey';

app.get('/encode/:encode', (req, res) => {
    const word = req.params.encode;
    const encodeResult = Caesar.Cipher(key).crypt(word);
    res.send(encodeResult);
});

app.get('/decode/:decode', (req, res) => {
    const decode = req.params.decode;
    const decodeResult = Caesar.Decipher(key).crypt(decode);
    res.send(decodeResult);
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});